package com.linkesoft.photocloud;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.linkesoft.photocloud.databinding.ActivityPhotoBinding;
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.RemoveFileRemoteOperation;
import com.owncloud.android.lib.resources.files.UploadFileRemoteOperation;

import java.io.File;

public class PhotoActivity extends AppCompatActivity {

    private File photoFile;
    private ActivityPhotoBinding binding;
    public static final String EXTRA_PHOTOFILE="photoFile";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= ActivityPhotoBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        photoFile = (File)getIntent().getExtras().get(EXTRA_PHOTOFILE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Bitmap bitmap = BitmapFactory.decodeFile(photoFile.getAbsolutePath());
        binding.image.setImageBitmap(bitmap);
    }

//    @Override
//    public boolean onSupportNavigateUp() {
//        onBackPressed();
//        return true;
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_photo, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean loggedIn = NextcloudLogin.isLoggedIn(this);
        menu.findItem(R.id.action_upload).setEnabled(loggedIn);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_delete) {
            new android.app.AlertDialog.Builder(this).setTitle(R.string.action_delete).setMessage(photoFile.getName()).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    delete();
                }
            }).show();
            return true;
        } else if (id == R.id.action_upload) {
            upload();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    void upload() {
        binding.progress.setVisibility(View.VISIBLE);
        Long timestamp = photoFile.lastModified() / 1000; // in s
        String remotePath = new File(MainActivity.remotePhotoDir,photoFile.getName()).getAbsolutePath();
        UploadFileRemoteOperation op = new UploadFileRemoteOperation(photoFile.getAbsolutePath(), remotePath, "image/jpg", timestamp.toString());
        op.execute(NextcloudLogin.getClient(this), new OnRemoteOperationListener() {
            @Override
            public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {
                binding.progress.setVisibility(View.GONE);
                if(result.isSuccess()) {
                    MainActivity.markUploaded(getApplicationContext(),photoFile);
                    finish();
                } else if(result.isException()) {
                    Toast.makeText(getApplicationContext(),result.getException().getLocalizedMessage(),Toast.LENGTH_LONG).show();
                }

            }
        }, new Handler());
    }

    void delete() {
        photoFile.delete();
        if (NextcloudLogin.isLoggedIn(this)) {
            binding.progress.setVisibility(View.VISIBLE);
            String remotePath = new File(MainActivity.remotePhotoDir,photoFile.getName()).getAbsolutePath();
            RemoveFileRemoteOperation op = new RemoveFileRemoteOperation(remotePath);
            op.execute(NextcloudLogin.getClient(this), new OnRemoteOperationListener() {
                @Override
                public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {
                    binding.progress.setVisibility(View.GONE);
                    if(result.isSuccess()) {
                        finish();
                    } else if(result.isException())
                        Toast.makeText(getApplicationContext(),result.getException().getLocalizedMessage(),Toast.LENGTH_LONG).show();

                }
            }, new Handler());
        } else {
            finish();
        }
    }

}
