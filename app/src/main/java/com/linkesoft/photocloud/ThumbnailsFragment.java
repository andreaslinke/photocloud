package com.linkesoft.photocloud;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.linkesoft.photocloud.databinding.ThumbnailsBinding;

import org.jetbrains.annotations.NotNull;

import java.io.File;

/**
 * A fragment representing a list of Files.
 * <p/>
 * Activities containing this fragment MUST implement the {@link ThumbnailsFragmentInteractionListener}
 * interface.
 */
public class ThumbnailsFragment extends Fragment {
    public interface ThumbnailsFragmentInteractionListener {
        void onThumbnailSelected(File item);
    }

    public final int columnCount = 3;
    public ThumbnailsRecyclerViewAdapter adapter;

    private ThumbnailsBinding binding;
    private ThumbnailsFragmentInteractionListener listener;
    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ThumbnailsFragment() {
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = ThumbnailsBinding.inflate(inflater, container, false);
        RecyclerView view = binding.getRoot();

        // Set the layout and adapter
        Context context = view.getContext();
        binding.list.setLayoutManager(new GridLayoutManager(context, columnCount));
        adapter = new ThumbnailsRecyclerViewAdapter(listener);
        binding.list.setAdapter(adapter);
        return view;
    }


    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        if (context instanceof ThumbnailsFragmentInteractionListener) {
            listener = (ThumbnailsFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPhotoFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
